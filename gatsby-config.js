module.exports = {
  plugins: [
    "gatsby-plugin-styled-components",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-transformer-json",
      options: {
        typeName: "PugsData",
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "data",
        path: `${__dirname}/data`,
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: `${__dirname}/images`,
      },
    },
    {
      resolve: "gatsby-plugin-sharp",
      options: {
        base64Width: 42,
        forceBase64Format: "webp",
      },
    },
  ],
}
