import React from "react"

import { createGlobalStyle } from "styled-components"
import modernNormalize from "styled-modern-normalize"

const ModernNormalize = createGlobalStyle`
  ${modernNormalize}
`

// The gatsby wrapping div element, anything that all pages share in common
// should be added here
const Layout = ({ children }) => (
  <>
    <ModernNormalize />
    {children}
  </>
)

export default Layout
