import { graphql, useStaticQuery } from "gatsby"

const query = graphql`
fragment ImageData_tall on PugsDataConnection {
  nodes {
    image_id
    image_type
    localFile {
      childImageSharp {
        fluid(maxHeight: 800) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
}

fragment ImageData_wide on PugsDataConnection {
  nodes {
    image_id
    image_type
    localFile {
      childImageSharp {
        fluid(maxWidth: 800) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
}

fragment ImageData_big on PugsDataConnection {
  nodes {
    image_id
    image_type
    localFile {
      childImageSharp {
        fluid(maxHeight: 800) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
}

fragment ImageData_small on PugsDataConnection {
  nodes {
    image_id
    image_type
    localFile {
      childImageSharp {
        fluid(maxWidth: 550) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }
}

query {
  tall: allPugsData(filter: {image_type: {eq: "tall"}}) {
    ...ImageData_tall
  }
  wide: allPugsData(filter: {image_type: {eq: "wide"}}) {
    ...ImageData_wide
  }
  big: allPugsData(filter: {image_type: {eq: "big"}}) {
    ...ImageData_big
  }
  normal: allPugsData(filter: {image_type: {eq: "normal"}}) {
    ...ImageData_small
  }
}
`

// useImageData
export default () => useStaticQuery(query)
