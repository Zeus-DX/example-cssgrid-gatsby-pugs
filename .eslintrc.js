module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: ["plugin:react/recommended", "airbnb"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: "module",
  },
  plugins: ["react"],
  rules: {
    // The airbnb config sets `max-len` rule to 100, so if using Prettier it is
    // important to match that in `.prettierrc` with `printWidth` of `100` too.
    //
    // Unlike with Prettier, if an eslint fix is applied, and it causes the line
    // length to exceed the allowed maximum, then the user is required to fix it.
    // https://eslint.org/docs/rules/max-len

    // Semi-colons are generally visual noise and more important for the parser
    // than a human. The linter is designed to catch ASI errors where possible.
    // Tempted to consider enforcing them for parity with rust, but avoiding for now.
    // https://eslint.org/docs/rules/semi
    semi: ["error", "never", { beforeStatementContinuationChars: "never" }],

    // I've opted for double quotes as this is more common across other languages.
    // They can disambiguate against backticks usage easily.
    // Typing single quotes is fine as the rule can auto fix to double.
    // https://eslint.org/docs/rules/quotes
    quotes: ["error", "double", { avoidEscape: true }],

    // Alignment whitespace for assignments can be useful, or appear to be at a glance.
    // It's not great for larger codebases with multiple collaborators.
    // Depending on content/context, the alignment may negatively impact readability
    // and can contribute noisey commits via alignment adjustment diffs those
    // related commits affecting git blame.
    // In most cases, you will prefer key/value pair readability and find vertical
    // alignment caters to scanning vertically or only serving an aesthetic purpose.
    //
    // Unfortunately, without using disable comment rules in the code, the linter
    // or formatter isn't going to be able to judge when it's appropriate.
    // I have disabled the rule to retain the vertical alignment I care about.
    // The rule could be set to "warn" to help catch other cases, it will allow
    // alignment for `=`, but only in the sense of spacing, it doesn't recognize
    // any actual vertical alignment.
    // https://eslint.org/docs/rules/no-multi-spaces
    "no-multi-spaces": ["off", { exceptions: { VariableDeclarator: true } }],
    // Adjusts airbnb rule to also permit aligning by value in objects.
    // Disables rule to retain my vertical alignment usage.
    // https://eslint.org/docs/rules/key-spacing
    "key-spacing": ["off", { beforeColon: false, afterColon: true, align: "value" }],
    // `no-multi-spaces` covers this, unless you want to target only JSX with it.
    // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-props-no-multi-spaces.md
    "react/jsx-props-no-multi-spaces": "off",

    // Would be ideal as ["error", "as-needed"], but allowing for omission
    // of parens when provided as an argument, eg `state => state.prop`
    "arrow-parens": "off",

    // Deviates from airbnb by not permitting in functions.
    // Akin to Prettier "es5" value
    "comma-dangle": ["error", {
      arrays: "always-multiline",
      objects: "always-multiline",
      imports: "always-multiline",
      exports: "always-multiline",
      functions: "never",
    }],

    // Disables prevention of mutating props on function params.
    // airbnb enables it with a list of exclusions in their `best-practices` ruleset.
    "no-param-reassign": ["error", { props: false }],

    // This rule converts string concatenation expressions to a template string.
    // Could be "warn" but for personal projects I'll generally use template
    // strings where it makes most sense, otherwise a concat expression.
    "prefer-template": "off",

    // Removes parens when not required, unless it's multi-line JSX.
    // Differs from airbnb which is more fine-tuned, but presently ignores JSX,
    // which is invalid as the suggested alternative cannot reproduce this behaviour:
    // https://github.com/airbnb/javascript/issues/2209
    "no-extra-parens": ["error", "all", { ignoreJSX: "multi-line" }],

    "no-unused-vars": "warn",
    "object-curly-newline": ["error", { multiline: true, consistent: true }],
    "react/jsx-closing-bracket-location": ["error", "line-aligned"],
    "react/jsx-filename-extension": "off",
    "react/prop-types": "off",

    // Modifies the airbnb rule:
    camelcase: ["warn", {
      properties: "never",
      ignoreDestructuring: false,
    }],
  },
}
